<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="{!! url('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{!! url('bower_components/font-awesome/css/font-awesome.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{{ url('css/admin/custom.css') }}" />
<!-- Theme style -->
<link rel="stylesheet" href="{!! url('bower_components/AdminLTE/dist/css/AdminLTE.min.css') !!}">
<link rel="stylesheet" href="{!! url('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') !!}">
<link rel="stylesheet" href="{!! url('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{!! url('bower_components/bootstrap-sweetalert/dist/sweetalert.css') !!}"/>
